﻿using System.Drawing;

namespace Ingenie.RobotSpiders
{
    public class InstructionsSet
    {
        public InstructionsSet(Size gridSize, Position position, string instructions)
        {
            GridSize = gridSize;

            Position = position;

            Instructions = instructions;
        }


        public Size GridSize { get; internal set; }

        public Position Position { get; internal set; }

        public string Instructions { get; internal set; }
    }
}