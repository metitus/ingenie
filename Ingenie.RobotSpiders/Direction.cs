﻿namespace Ingenie.RobotSpiders
{
    public enum Direction
    {
        Right,
        Left,
        Up,
        Down
    }
}