﻿using System.Collections.Generic;
using System.Drawing;

namespace Ingenie.RobotSpiders
{
    public class MovementCoordinator
    {
        private Position _currentPosition;
        private List<Direction> _matrix;
        private Size _gridSize;


        public void Initialize(Size gridSize, Position position)
        {
            _currentPosition = position;
            _gridSize = gridSize;
            _matrix = new List<Direction>
            {
                 Direction.Up,
                 Direction.Left,
                 Direction.Down,
                 Direction.Right
            };
        }


        public Position RotateLeft()
        {
            Rotate(_matrix.IndexOf(_currentPosition.Direction) + 1, 4, 0);

            return _currentPosition;
        }

        public Position RotateRight()
        {
            Rotate(_matrix.IndexOf(_currentPosition.Direction) - 1, -1, 3);

            return _currentPosition;
        }

        public void Rotate(int index, int boundaryIndex, int resetIndex)
        {
            if (index == boundaryIndex)
            {
                index = resetIndex;
            }

            _currentPosition.Direction = _matrix[index];
        }

        public Position MoveForward()
        {
            switch (_currentPosition.Direction)
            {
                case Direction.Up:

                    MoveUp();

                    break;

                case Direction.Right:

                    MoveRight();

                    break;

                case Direction.Down:

                    MoveDown();

                    break;

                default:

                    MoveLeft();

                    break;
            }

            return _currentPosition;
        }

        private void MoveUp()
        {
            if (_currentPosition.Coordinates.Y + 1 <= _gridSize.Height)
            {
                _currentPosition.Coordinates = new Point(_currentPosition.Coordinates.X, _currentPosition.Coordinates.Y + 1);
            }
        }

        private void MoveRight()
        {
            if (_currentPosition.Coordinates.X + 1 <= _gridSize.Width)
            {
                _currentPosition.Coordinates = new Point(_currentPosition.Coordinates.X + 1, _currentPosition.Coordinates.Y);
            }
        }

        private void MoveDown()
        {
            if (_currentPosition.Coordinates.Y - 1 >= 0)
            {
                _currentPosition.Coordinates = new Point(_currentPosition.Coordinates.X, _currentPosition.Coordinates.Y - 1);
            }
        }

        private void MoveLeft()
        {
            if (_currentPosition.Coordinates.X - 1 >= 0)
            {
                _currentPosition.Coordinates = new Point(_currentPosition.Coordinates.X - 1, _currentPosition.Coordinates.Y);
            }
        }
    }
}