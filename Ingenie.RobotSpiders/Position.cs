﻿using System.Drawing;

namespace Ingenie.RobotSpiders
{
    public class Position
    {
        public Position(Point coordinates, Direction direction)
        {
            Coordinates = coordinates;

            Direction = direction;
        }


        public Point Coordinates { get; internal set; }

        public Direction Direction { get; internal set; }
    }
}