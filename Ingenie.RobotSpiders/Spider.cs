﻿using System.Drawing;

namespace Ingenie.RobotSpiders
{
    public class Spider
    {
        private readonly MovementCoordinator _movementCoordinator;
        private readonly IntructionsBatchParser _intructionsBatchParser;


        public Spider(MovementCoordinator movementCoordinator, IntructionsBatchParser intructionsBatchParser)
        {
            _movementCoordinator = movementCoordinator;
            _intructionsBatchParser = intructionsBatchParser;
        }

        public Spider(Size gridSize, Position position, MovementCoordinator movementCoordinator)
        {
            _movementCoordinator = movementCoordinator;

            _movementCoordinator.Initialize(gridSize, position);
        }


        public Position Execute(string instructions)
        {
            Position position = null;

            foreach (var instruction in instructions.ToUpper())
            {
                if (instruction == 'L')
                {
                    position =  _movementCoordinator.RotateLeft(); continue;
                }

                if (instruction == 'R')
                {
                    position = _movementCoordinator.RotateRight(); continue;
                }

                position = _movementCoordinator.MoveForward();
            }

            return position;
        }

        public string ExecuteBatch(string batchOfInstructions)
        {
            var instructionsSet = _intructionsBatchParser.Parse(batchOfInstructions);
            var initialPosition = new Position(instructionsSet.Position.Coordinates,
                instructionsSet.Position.Direction);

            _movementCoordinator.Initialize(instructionsSet.GridSize, initialPosition);

            var position = Execute(instructionsSet.Instructions);

            return $"{position.Coordinates.X} {position.Coordinates.Y} {position.Direction.ToString()}";
        }
    }
}