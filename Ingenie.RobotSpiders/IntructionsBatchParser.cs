﻿using System;
using System.Drawing;

namespace Ingenie.RobotSpiders
{
    public class IntructionsBatchParser
    {
        public InstructionsSet Parse(string batchOfInstructions)
        {
            var linesOfInstructions = batchOfInstructions.Split(new[] { Environment.NewLine },
                StringSplitOptions.None);
            var gridSize = ExtractGridSize(linesOfInstructions[0]);
            var initialPosition = ExtractInitialPosition(linesOfInstructions[1]);

            return new InstructionsSet(gridSize, initialPosition, linesOfInstructions[2]);
        }

        private Size ExtractGridSize(string line)
        {
            var array = line.Split(' ');

            return new Size(int.Parse(array[0]), int.Parse (array[1]));
        }

        private Position ExtractInitialPosition(string line)
        {
            var array = line.Split(' ');
            var x = int.Parse(array[0]);
            var y = int.Parse(array[1]);
            var direction = (Direction)Enum.Parse(typeof(Direction), array[2]);

            return new Position(new Point(x, y), direction);
        }
    }
}