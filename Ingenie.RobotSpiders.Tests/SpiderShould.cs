﻿using NUnit.Framework;
using System;
using System.Drawing;
using System.Text;

namespace Ingenie.RobotSpiders.Tests
{
    public class SpiderShould
    {
        [Test]
        public void BeAtTheExpectedCoordinatesAndHeading_WhenGivenAsetOfInstructions()
        {
            var initialPosition = new Position(new Point(2, 4), Direction.Left);
            var spider = new Spider(new Size(7, 15), initialPosition, new MovementCoordinator());

            var position = spider.Execute("FLFLFRFFLF");

            Assert.True(position.Coordinates.X == 3);
            Assert.True(position.Coordinates.Y == 1);
            Assert.True(position.Direction == Direction.Right);
        }

        [Test]
        public void BeAtTheExpectedCoordinatesAndHeading_WhenGivenABatchOfInstructions()
        {
            var instruction = new StringBuilder();
            instruction.Append("7 15");
            instruction.Append(Environment.NewLine);
            instruction.Append("2 4 Left");
            instruction.Append(Environment.NewLine);
            instruction.Append("FLFLFRFFLF");

            var spider = new Spider(new MovementCoordinator(), new IntructionsBatchParser());

            Assert.AreEqual(spider.ExecuteBatch(instruction.ToString()), "3 1 Right");
        }
    }
}