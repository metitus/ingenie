﻿using NUnit.Framework;
using System.Drawing;

namespace Ingenie.RobotSpiders.Tests
{
    public class MovementCoordinatorShould
    {
        [TestCase(Direction.Down, Direction.Right)]
        [TestCase(Direction.Right, Direction.Up)]
        [TestCase(Direction.Up, Direction.Left)]
        [TestCase(Direction.Left, Direction.Down)]
        public void PositionTheSpiderAccordingly_WhenRotatingLeft(Direction initialDirection, Direction expectedDirection)
        {
            var movementCoordinatorTests = new MovementCoordinator();
            var initialPosition = new Position(new Point(1, 1), initialDirection);
            movementCoordinatorTests.Initialize(new Size(1, 1), initialPosition);

            var position = movementCoordinatorTests.RotateLeft();

            Assert.AreEqual(expectedDirection, position.Direction);
        }

        [TestCase(Direction.Down, Direction.Left)]
        [TestCase(Direction.Left, Direction.Up)]
        [TestCase(Direction.Up, Direction.Right)]
        [TestCase(Direction.Right, Direction.Down)]
        public void PositionTheSpiderAccordingly_WhenRotatingRight(Direction initialDirection, Direction expectedDirection)
        {
            var movementCoordinatorTests = new MovementCoordinator();
            var initialPosition = new Position(new Point(1, 1), initialDirection);
            movementCoordinatorTests.Initialize(new Size(1, 1), initialPosition);

            var position = movementCoordinatorTests.RotateRight();

            Assert.AreEqual(expectedDirection, position.Direction);
        }

        [TestCase(1, 1, 1, 2, Direction.Up)]
        [TestCase(1, 1, 2, 1, Direction.Right)]
        [TestCase(2, 2, 2, 1, Direction.Down)]
        [TestCase(2, 2, 1, 2, Direction.Left)]
        public void PositionTheSpiderAccordingly_WhenMovingForwardAndWithinBounderies(int initialX, int initialY,
            int expectedX, int expectedY, Direction direction)
        {
            var movementCoordinatorTests = new MovementCoordinator();
            var initialPosition = new Position(new Point(initialX, initialY), direction);
            movementCoordinatorTests.Initialize(new Size(10, 10), initialPosition);

            var position = movementCoordinatorTests.MoveForward();

            Assert.AreEqual(expectedX, position.Coordinates.X);
            Assert.AreEqual(expectedY, position.Coordinates.Y);
        }

        [TestCase(0, 0, 0, 0, Direction.Up)]
        [TestCase(0, 0, 0, 0, Direction.Right)]
        [TestCase(0, 0, 0, 0, Direction.Down)]
        [TestCase(0, 0, 0, 0, Direction.Left)]
        public void KeepTheSpiderInTheSamePosition_WhenMovingForwardAndOutsideTheBounderies(int initialX, int initialY,
            int expectedX, int expectedY, Direction direction)
        {
            var movementCoordinatorTests = new MovementCoordinator();
            var initialPosition = new Position(new Point(initialX, initialY), direction);
            movementCoordinatorTests.Initialize(new Size(0, 0), initialPosition);

            var position = movementCoordinatorTests.MoveForward();

            Assert.AreEqual(expectedX, position.Coordinates.X);
            Assert.AreEqual(expectedY, position.Coordinates.Y);
        }
    }
}