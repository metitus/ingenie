﻿using NUnit.Framework;
using System;
using System.Text;

namespace Ingenie.RobotSpiders.Tests
{
    public class IntructionsBatchParserShould
    {
        [Test]
        public void MatchTheInitialPositionSpecified_WhenParsingASetOfInstructions()
        {
            var IntructionsBatch = new IntructionsBatchParser();
            var instruction = new StringBuilder();
            instruction.Append("7 15");
            instruction.Append(Environment.NewLine);
            instruction.Append("2 4 Left");
            instruction.Append(Environment.NewLine);
            instruction.Append("FLFLFRFFLF");

            var instructionsSet = IntructionsBatch.Parse(instruction.ToString());

            Assert.AreEqual(2, instructionsSet.Position.Coordinates.X);
            Assert.AreEqual(4, instructionsSet.Position.Coordinates.Y);
        }

        [Test]
        public void MatchTheDirectionSpecified_WhenParsingASetOfInstructions()
        {
            var IntructionsBatch = new IntructionsBatchParser();
            var instruction = new StringBuilder();
            instruction.Append("7 15");
            instruction.Append(Environment.NewLine);
            instruction.Append("2 4 Left");
            instruction.Append(Environment.NewLine);
            instruction.Append("FLFLFRFFLF");

            var instructionsSet = IntructionsBatch.Parse(instruction.ToString());

            Assert.AreEqual(Direction.Left, instructionsSet.Position.Direction);
        }

        [Test]
        public void MatchTheGridSizeSpecified_WhenParsingASetOfInstructions()
        {
            var IntructionsBatch = new IntructionsBatchParser();
            var instruction = new StringBuilder();
            instruction.Append("7 15");
            instruction.Append(Environment.NewLine);
            instruction.Append("2 4 Left");
            instruction.Append(Environment.NewLine);
            instruction.Append("FLFLFRFFLF");

            var instructionsSet = IntructionsBatch.Parse(instruction.ToString());

            Assert.AreEqual(7, instructionsSet.GridSize.Width);
            Assert.AreEqual(15, instructionsSet.GridSize.Height);

        }
    }
}
