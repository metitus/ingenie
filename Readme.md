Assumptions:

- Assumed that the input format as always valid
- The grid size was specified as an input, but no behaviour was specified in case a forward movement places the spider out of bounderies, so the added behaviour was to ignore that movement
- I was not sure if I should parse the instructions as a string, so I added that as a second feature

Design decisions:

- For the setup of the tests, I used the arrange, act, assert layout
- Followed the rules of TDD and the red, gree, refactoring cicle (git commits)
- Followed Kent Beck design rules (Passes the tests, Reveals intention, No duplication, Fewest elements)

Source code/TDD steps

https://bitbucket.org/metitus/ingenie/src